class UserEquipment < ActiveRecord::Base
  belongs_to :user
  belongs_to :equipment

  validates_presence_of :user
  validates_presence_of :equipment
end

# == Schema Information
#
# Table name: user_equipments
#
#  id           :integer          not null, primary key
#  user_id      :integer
#  equipment_id :integer
#  created_at   :datetime
#  updated_at   :datetime
#
# Indexes
#
#  index_user_equipments_on_equipment_id  (equipment_id)
#  index_user_equipments_on_user_id       (user_id)
#
