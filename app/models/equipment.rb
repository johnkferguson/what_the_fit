class Equipment < ActiveRecord::Base
  has_many :exercise_equipments
  has_many :exercises, through: :exercise_equipments
  has_many :user_equipments
  has_many :users, through: :user_equipments

  validates_presence_of :name
  validates_uniqueness_of :name, case_insensitive: true
end

# == Schema Information
#
# Table name: equipment
#
#  id           :integer          not null, primary key
#  name         :string(255)
#  description  :text
#  product_link :string(255)
#  created_at   :datetime
#  updated_at   :datetime
#
