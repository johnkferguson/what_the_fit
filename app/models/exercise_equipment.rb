class ExerciseEquipment < ActiveRecord::Base
  belongs_to :exercise
  belongs_to :equipment

  validates_presence_of :exercise
  validates_presence_of :equipment
end

# == Schema Information
#
# Table name: exercise_equipments
#
#  id           :integer          not null, primary key
#  exercise_id  :integer
#  equipment_id :integer
#  created_at   :datetime
#  updated_at   :datetime
#
# Indexes
#
#  index_exercise_equipments_on_equipment_id  (equipment_id)
#  index_exercise_equipments_on_exercise_id   (exercise_id)
#
