class User < ActiveRecord::Base
  devise :database_authenticatable, :registerable, :recoverable, :rememberable,
         :trackable

  has_many :created_workouts, class_name: "Workout", foreign_key: "creator_id"
  has_many :user_equipments
  has_many :equipments, through: :user_equipments
  has_many :favorites
  has_many :favorite_workouts, through: :favorites,
                               source: :favoritable,
                               source_type: "Workout"
  has_many :favorite_exercises, through: :favorites,
                               source: :favoritable,
                               source_type: "Exercise"

  validates_presence_of :email
  validates_uniqueness_of :email, case_insensitive: true
  validates_presence_of :password
  validates_length_of :password, in: 6..20

end

# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string(255)      default(""), not null
#  encrypted_password     :string(255)      default(""), not null
#  reset_password_token   :string(255)
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string(255)
#  last_sign_in_ip        :string(255)
#  created_at             :datetime
#  updated_at             :datetime
#  admin                  :boolean          default(FALSE)
#
# Indexes
#
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#
