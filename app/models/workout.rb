class Workout < ActiveRecord::Base
  belongs_to :creator, class_name: "User"
  has_many :workout_exercises
  has_many :exercises, through: :workout_exercises
  has_many :favorites, as: :favoritable
  has_many :favoriters, through: :favorites, source: :user

  validates_presence_of :creator
  validates_presence_of :name
end

# == Schema Information
#
# Table name: workouts
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  creator_id :integer
#  created_at :datetime
#  updated_at :datetime
#
# Indexes
#
#  index_workouts_on_creator_id  (creator_id)
#
