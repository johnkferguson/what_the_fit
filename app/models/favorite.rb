class Favorite < ActiveRecord::Base
  belongs_to :user
  belongs_to :favoritable, polymorphic: true

  validates_presence_of :user
  validates_presence_of :favoritable_type
  validates_presence_of :favoritable_id
end

# == Schema Information
#
# Table name: favorites
#
#  id               :integer          not null, primary key
#  user_id          :integer
#  favoritable_id   :integer
#  favoritable_type :string(255)
#  created_at       :datetime
#  updated_at       :datetime
#
# Indexes
#
#  index_favorites_on_user_id  (user_id)
#
