class WorkoutExercise < ActiveRecord::Base
  belongs_to :workout
  belongs_to :exercise

  validates_presence_of :workout
  validates_presence_of :exercise
end

# == Schema Information
#
# Table name: workout_exercises
#
#  id          :integer          not null, primary key
#  workout_id  :integer
#  exercise_id :integer
#  created_at  :datetime
#  updated_at  :datetime
#
# Indexes
#
#  index_workout_exercises_on_exercise_id  (exercise_id)
#  index_workout_exercises_on_workout_id   (workout_id)
#
