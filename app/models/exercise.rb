class Exercise < ActiveRecord::Base
  has_many :exercise_equipments
  has_many :equipments, through: :exercise_equipments
  has_many :workout_exercises
  has_many :workouts, through: :workout_exercises
  has_many :favorites, as: :favoritable
  has_many :favoriters, through: :favorites, source: :user

  validates_presence_of :name
  validates_uniqueness_of :name, case_insensitive: true
end

# == Schema Information
#
# Table name: exercises
#
#  id          :integer          not null, primary key
#  name        :string(255)
#  description :text
#  symmetrical :boolean          default(FALSE)
#  created_at  :datetime
#  updated_at  :datetime
#
