require 'spec_helper'

describe Exercise do
  it "has a valid factory" do
    expect(build(:exercise)).to be_valid
  end

  describe "associations" do
    it { should have_many(:exercise_equipments) }
    it { should have_many(:equipments).through(:exercise_equipments) }
    it { should have_many(:workout_exercises) }
    it { should have_many(:workouts).through(:workout_exercises) }
    it { should validate_presence_of(:name) }
    it { should validate_uniqueness_of(:name) }

    describe "#favorites" do
      it "returns the favoritings of an exercise" do
        exercise = create(:exercise)
        favorite = create(:favorite, :exercise, favoritable_id: exercise.id)
        different_fav = create(:favorite, :exercise)
        expect(exercise.favorites).to match_array([favorite])
      end
    end

    describe "#favoriters" do
      it "returns the favoriters of an exercise" do
        exercise = create(:exercise)
        user = create(:user)
        favorite = create(:favorite, :exercise,
          favoritable_id: exercise.id, user: user)

        expect(exercise.favoriters).to match_array([user])
      end
    end
  end

end

# == Schema Information
#
# Table name: exercises
#
#  id          :integer          not null, primary key
#  name        :string(255)
#  description :text
#  symmetrical :boolean          default(FALSE)
#  created_at  :datetime
#  updated_at  :datetime
#
