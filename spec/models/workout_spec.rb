require 'spec_helper'

describe Workout do
  it "has a valid factory" do
    expect(build(:workout)).to be_valid
  end

  describe "associations" do
    it { should belong_to(:creator).class_name("User") }
    it { should have_many(:workout_exercises) }
    it { should have_many(:exercises).through(:workout_exercises) }

    describe "#favorites" do
      it "returns the favoritings of a workout" do
        workout = create(:workout)
        favorite = create(:favorite, :workout, favoritable_id: workout.id)
        different_fav = create(:favorite, :workout)
        expect(workout.favorites).to match_array([favorite])
      end
    end

    describe "#favoriters" do
      it "returns the favoriters of a workout" do
        workout = create(:workout)
        user = create(:user)
        favorite = create(:favorite, :workout,
          favoritable_id: workout.id, user: user)

        expect(workout.favoriters).to match_array([user])
      end
    end
  end

  it { should validate_presence_of(:creator) }
  it { should validate_presence_of(:name) }
end

# == Schema Information
#
# Table name: workouts
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  creator_id :integer
#  created_at :datetime
#  updated_at :datetime
#
# Indexes
#
#  index_workouts_on_creator_id  (creator_id)
#
