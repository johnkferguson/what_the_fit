require 'spec_helper'

describe ExerciseEquipment do
  it "should have a valid factory" do
    expect(build(:exercise_equipment)).to be_valid
  end

  it { should belong_to(:exercise) }
  it { should belong_to(:equipment) }
  it { should validate_presence_of(:exercise) }
  it { should validate_presence_of(:equipment) }

end

# == Schema Information
#
# Table name: exercise_equipments
#
#  id           :integer          not null, primary key
#  exercise_id  :integer
#  equipment_id :integer
#  created_at   :datetime
#  updated_at   :datetime
#
# Indexes
#
#  index_exercise_equipments_on_equipment_id  (equipment_id)
#  index_exercise_equipments_on_exercise_id   (exercise_id)
#
