require 'spec_helper'

describe WorkoutExercise do
  it "should have a valid factory" do
    expect(build(:workout_exercise)).to be_valid
  end

  it { should belong_to(:workout) }
  it { should belong_to(:exercise) }
  it { should validate_presence_of(:workout) }
  it { should validate_presence_of(:exercise) }
end

# == Schema Information
#
# Table name: workout_exercises
#
#  id          :integer          not null, primary key
#  workout_id  :integer
#  exercise_id :integer
#  created_at  :datetime
#  updated_at  :datetime
#
# Indexes
#
#  index_workout_exercises_on_exercise_id  (exercise_id)
#  index_workout_exercises_on_workout_id   (workout_id)
#
