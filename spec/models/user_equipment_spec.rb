require 'spec_helper'

describe UserEquipment do
  it "should have a valid factory" do
    expect(build(:user_equipment)).to be_valid
  end

  it { should belong_to(:user) }
  it { should belong_to(:equipment) }
  it { should validate_presence_of(:user) }
  it { should validate_presence_of(:equipment) }

end

# == Schema Information
#
# Table name: user_equipments
#
#  id           :integer          not null, primary key
#  user_id      :integer
#  equipment_id :integer
#  created_at   :datetime
#  updated_at   :datetime
#
# Indexes
#
#  index_user_equipments_on_equipment_id  (equipment_id)
#  index_user_equipments_on_user_id       (user_id)
#
