require 'spec_helper'

describe Equipment do
  it "has a valid factory" do
    expect(build(:equipment)).to be_valid
  end

  it { should have_many(:exercise_equipments) }
  it { should have_many(:exercises).through(:exercise_equipments) }
  it { should have_many(:user_equipments) }
  it { should have_many(:users).through(:user_equipments) }
  it { should validate_presence_of(:name) }
  it { should validate_uniqueness_of(:name) }
end

# == Schema Information
#
# Table name: equipment
#
#  id           :integer          not null, primary key
#  name         :string(255)
#  description  :text
#  product_link :string(255)
#  created_at   :datetime
#  updated_at   :datetime
#
