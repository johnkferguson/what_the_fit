require 'spec_helper'

describe User do

  it "has a valid factory" do
    expect(build(:user)).to be_valid
  end


  describe "associations" do
    let(:user) { create(:user) }

    it { should have_many(:created_workouts).class_name("Workout")
          .with_foreign_key("creator_id") }
    it { should have_many(:user_equipments) }
    it { should have_many(:equipments).through(:user_equipments) }
    it { should have_many(:favorites) }

    describe "#favorite_workouts" do
      it "returns a user's favorite workouts" do
        fav_workout = create(:workout)
        unfav_workout = create(:workout)
        favorite = create(:favorite, :workout,
                    favoritable_id: fav_workout.id, user: user)
        expect(user.favorite_workouts).to match_array([fav_workout])
      end
    end

    describe "#favorite_exercises" do
      it "returns a user's favorite exercises" do
        fav_exercise = create(:exercise)
        unfav_exercise = create(:exercise)
        favorite = create(:favorite, :exercise,
                    favoritable_id: fav_exercise.id, user: user)
        expect(user.favorite_exercises).to match_array([fav_exercise])
      end
    end
  end

  it { should validate_uniqueness_of(:email).case_insensitive }
  it { should validate_presence_of(:email) }
  it { should validate_presence_of(:password) }
  it { should ensure_length_of(:password).is_at_least(6).is_at_most(20) }

  describe "#admin?" do
    context "when the user is not an admin" do
      it "returns false" do
        user = build(:user)
        expect(user.admin?).to be(false)
      end
    end

    context "when the user is an admin" do
      it "returns true" do
        user = build(:user, :admin)
        expect(user.admin?).to be(true)
      end
    end

  end
end

# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string(255)      default(""), not null
#  encrypted_password     :string(255)      default(""), not null
#  reset_password_token   :string(255)
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string(255)
#  last_sign_in_ip        :string(255)
#  created_at             :datetime
#  updated_at             :datetime
#  admin                  :boolean          default(FALSE)
#
# Indexes
#
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#
