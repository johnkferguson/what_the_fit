require 'spec_helper'

describe Favorite do
  it "should have a valid factory" do
    expect(build(:favorite)).to be_valid
  end

  it { should belong_to(:user) }
  it { should belong_to(:favoritable) }

  it { should validate_presence_of(:user) }
  it { should validate_presence_of(:favoritable_type) }
  it { should validate_presence_of(:favoritable_id) }
end

# == Schema Information
#
# Table name: favorites
#
#  id               :integer          not null, primary key
#  user_id          :integer
#  favoritable_id   :integer
#  favoritable_type :string(255)
#  created_at       :datetime
#  updated_at       :datetime
#
# Indexes
#
#  index_favorites_on_user_id  (user_id)
#
