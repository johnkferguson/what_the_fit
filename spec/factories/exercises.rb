# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :exercise do
    name { Faker::Product.product_name }
    description { Faker::Lorem.sentence(10) }
    symmetrical false
  end
end

# == Schema Information
#
# Table name: exercises
#
#  id          :integer          not null, primary key
#  name        :string(255)
#  description :text
#  symmetrical :boolean          default(FALSE)
#  created_at  :datetime
#  updated_at  :datetime
#
