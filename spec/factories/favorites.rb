# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :favorite do
    user
    sequence :favoritable_id do |n|
      n
    end
    favoritable_type { Faker::Lorem.characters(10) }

    trait :workout do
      favoritable_type "Workout"
    end

    trait :exercise do
      favoritable_type "Exercise"
    end
  end
end

# == Schema Information
#
# Table name: favorites
#
#  id               :integer          not null, primary key
#  user_id          :integer
#  favoritable_id   :integer
#  favoritable_type :string(255)
#  created_at       :datetime
#  updated_at       :datetime
#
# Indexes
#
#  index_favorites_on_user_id  (user_id)
#
