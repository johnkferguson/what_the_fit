# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :workout do
    name { Faker::Product.product_name }
    association :creator, factory: :user, strategy: :build
  end
end

# == Schema Information
#
# Table name: workouts
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  creator_id :integer
#  created_at :datetime
#  updated_at :datetime
#
# Indexes
#
#  index_workouts_on_creator_id  (creator_id)
#
