# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :user_equipment do
    user
    equipment
  end
end

# == Schema Information
#
# Table name: user_equipments
#
#  id           :integer          not null, primary key
#  user_id      :integer
#  equipment_id :integer
#  created_at   :datetime
#  updated_at   :datetime
#
# Indexes
#
#  index_user_equipments_on_equipment_id  (equipment_id)
#  index_user_equipments_on_user_id       (user_id)
#
