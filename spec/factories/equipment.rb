# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :equipment do
    name { Faker::Product.product_name }
    description { Faker::Lorem.sentence(10) }
    product_link { Faker::Internet.http_url }
  end
end

# == Schema Information
#
# Table name: equipment
#
#  id           :integer          not null, primary key
#  name         :string(255)
#  description  :text
#  product_link :string(255)
#  created_at   :datetime
#  updated_at   :datetime
#
