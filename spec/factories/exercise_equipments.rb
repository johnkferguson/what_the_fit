# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :exercise_equipment do
    exercise
    equipment
  end
end

# == Schema Information
#
# Table name: exercise_equipments
#
#  id           :integer          not null, primary key
#  exercise_id  :integer
#  equipment_id :integer
#  created_at   :datetime
#  updated_at   :datetime
#
# Indexes
#
#  index_exercise_equipments_on_equipment_id  (equipment_id)
#  index_exercise_equipments_on_exercise_id   (exercise_id)
#
