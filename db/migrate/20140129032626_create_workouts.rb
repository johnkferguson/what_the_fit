class CreateWorkouts < ActiveRecord::Migration
  def change
    create_table :workouts do |t|
      t.string :name
      t.belongs_to :creator, index: true

      t.timestamps
    end
  end
end
