class CreateFavorites < ActiveRecord::Migration
  def change
    create_table :favorites do |t|
      t.references :user, index: true
      t.integer :favoritable_id
      t.string :favoritable_type

      t.timestamps
    end
  end
end
