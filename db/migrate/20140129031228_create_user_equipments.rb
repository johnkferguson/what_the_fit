class CreateUserEquipments < ActiveRecord::Migration
  def change
    create_table :user_equipments do |t|
      t.references :user, index: true
      t.references :equipment, index: true

      t.timestamps
    end
  end
end
