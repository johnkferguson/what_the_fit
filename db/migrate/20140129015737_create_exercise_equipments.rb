class CreateExerciseEquipments < ActiveRecord::Migration
  def change
    create_table :exercise_equipments do |t|
      t.references :exercise, index: true
      t.references :equipment, index: true

      t.timestamps
    end
  end
end
